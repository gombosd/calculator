import React, { ChangeEvent, FormEvent, useCallback, useState } from 'react';

type State = {
  loading: boolean;
  input: string;
  result: string;
};

export default function Index(): JSX.Element {
  const [state, setState] = useState<State>(() => ({ loading: false, input: '', result: '0' }));

  const handleChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      event.preventDefault();
      event.stopPropagation();
      const value = event.target.value;
      setState((state) => ({ ...state, input: value }));
    },
    [setState],
  );

  const handleSubmit = useCallback(
    async (event: FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      event.stopPropagation();
      setState((state) => ({ ...state, loading: true }));
      const response = await fetch('/api/calculate', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ input: state.input }),
      });
      const responseBody = (await response.json()) as { result: string };
      setState((state) => ({ ...state, loading: false, result: responseBody.result }));
    },
    [state, setState],
  );

  return (
    <div className="container">
      <div className="input">
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Type here..."
            disabled={state.loading}
            value={state.input}
            onChange={handleChange}
          />
          <button>=</button>
        </form>
      </div>
      <div className="result">
        <input type="text" readOnly={true} value={state.result} />
      </div>
    </div>
  );
}
