import contentType from 'content-type';
import { FastifyInstance, HTTPMethods } from 'fastify';
import { NextApiRequest, NextApiResponse } from 'next';
import getRawBody from 'raw-body';
import { createApp } from '../../server/server';

let app: FastifyInstance | null = null;

export const config = {
  api: {
    bodyParser: false,
  },
};

export default async (req: NextApiRequest, res: NextApiResponse): Promise<void> => {
  if (!app) {
    app = await createApp();
  }
  await app.ready();
  const response = await app.inject({
    method: req.method as HTTPMethods,
    url: req.url,
    payload: await getRawBody(req, {
      length: req.headers['content-length'] as string,
      limit: '1mb',
      encoding: req.headers['content-type'] ? contentType.parse(req).parameters.charset : undefined,
    }),
    headers: req.headers,
  });
  Object.entries(response.headers).forEach(([key, value]) => res.setHeader(key, getHeaderValue(value)));
  res.status(response.statusCode).send(response.rawPayload);
};

const getHeaderValue = (val: string | string[] | undefined | number): string =>
  Array.isArray(val) ? val.join(',') : val ? `${val}` : '';
