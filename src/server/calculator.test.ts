import 'jest';
import { evaluate } from './calculator';

const cases: { input: string; result: string }[] = [
  {
    input: '1+1',
    result: '2',
  },
  {
    input: '    1  + 1   ',
    result: '2',
  },
  {
    input: '1-1',
    result: '0',
  },
  {
    input: '1-2',
    result: '-1',
  },
  {
    input: '1.2+2.3',
    result: '3.5',
  },
  {
    input: '10*8',
    result: '80',
  },
  {
    input: '100/2',
    result: '50',
  },
  {
    input: '1+1+1',
    result: '3',
  },
  {
    input: '1+1+1+1',
    result: '4',
  },
  {
    input: '1+1-1',
    result: '1',
  },
  {
    input: '1+1*2',
    result: '3',
  },
  {
    input: '1+4/2',
    result: '3',
  },
  {
    input: '2/0',
    result: 'Infinity',
  },
  {
    input: '2/0*2+32',
    result: 'Infinity',
  },
  {
    input: '2/3*3',
    result: '2',
  },
];
cases.forEach(({ input, result }, i) =>
  it(`case ${i}`, () => {
    expect(evaluate(input)).toEqual(result);
  }),
);
