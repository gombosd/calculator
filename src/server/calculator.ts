type Operator = '*' | '/' | '+' | '-';
const OPERATORS: Operator[][] = [
  ['*', '/'],
  ['+', '-'],
];
const PROCESSORS: Record<Operator, (left: string, right: string) => string> = {
  '*': (left: string, right: string): string => `${parseFloat(left) * parseFloat(right)}`,
  '/': (left: string, right: string): string => `${parseFloat(left) / parseFloat(right)}`,
  '+': (left: string, right: string): string => `${parseFloat(left) + parseFloat(right)}`,
  '-': (left: string, right: string): string => `${parseFloat(left) - parseFloat(right)}`,
};

const TOKENIZER = /(([\d.]+)|([/*+-]))/g;

export const evaluate = (input: string): string => {
  const tokenized = Array.from(input.matchAll(TOKENIZER), ([val]) => val);
  return reduce(tokenized);
};

const reduce = (tokens: string[]): string => {
  let operatorIndex = -1;
  for (const op of OPERATORS) {
    operatorIndex = tokens.findIndex((token) => op.includes(token as Operator));
    if (operatorIndex !== -1) {
      break;
    }
  }
  if (operatorIndex === -1) {
    return tokens[0];
  }
  const operator = tokens[operatorIndex] as Operator;
  const left = tokens[operatorIndex - 1];
  const right = tokens[operatorIndex + 1];

  const newTokens = [
    ...tokens.slice(0, operatorIndex - 1),
    PROCESSORS[operator](left, right),
    ...tokens.slice(operatorIndex + 2),
  ];

  if (newTokens.length > 1) {
    return reduce(newTokens);
  }
  return newTokens[0];
};
