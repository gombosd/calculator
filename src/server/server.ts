import fastify, { FastifyError, FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { ApiError } from 'next/dist/next-server/server/api-utils';
import { evaluate } from './calculator';

type CalculateBody = {
  input: string;
};

type CalculateResponse = {
  response: string;
};

export const createApp = async (): Promise<FastifyInstance> => {
  const app = fastify({
    logger: false,
  });

  app.post<{ Body: CalculateBody; Response: CalculateResponse }>('/api/calculate', {
    schema: {
      body: {
        type: 'object',
        title: 'calculateBody',
        required: ['input'],
        properties: {
          input: { type: 'string' },
        },
        additionalProperties: false,
      },
      response: {
        200: {
          type: 'object',
          title: 'calculateResponse',
          required: ['result'],
          properties: {
            result: { type: 'string' },
          },
          additionalProperties: false,
        },
      },
    },
    async handler(req, reply) {
      const input = req.body.input;

      const result = evaluate(input);

      await reply.status(200).send({ result });
    },
  });

  app.setErrorHandler((error: ApiError | FastifyError, req: FastifyRequest, reply: FastifyReply): void => {
    req.log.error(error);
    if (error instanceof ApiError) {
      void reply.status(error.statusCode).send({ status: error.statusCode, response: { detail: error } });
      return;
    }
    void reply.status(500).send({ status: error.statusCode, response: { detail: error } });
    return;
  });

  return app;
};
